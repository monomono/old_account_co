#!/usr/bin/env python
#@author Jovany Leandro G.C <dirindesa@neurotec.co>
#@date 2017-05-24

import csv
import decimal
from collections import defaultdict
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.dom import minidom

#se hace un mapeo a las cuentas de tryton
#el kind de las cuentas es inferido de minimal account char,
#necesita evaluacion.
ODOO_TO_TRYTON_ACCOUNT_TYPE = {
    'account.data_account_type_liquidity': 'account_type_liquidity',
    'account.data_account_type_current_assets': 'account_type_current_assets',
    'account.data_account_type_receivable': {'type': {'ref': 'account_type_receivable'}, 'kind': 'receivable', 'deferral': True},
    'account.data_account_type_payable': {'type': {'ref': 'account_type_payable'}, 'kind': 'payable', 'deferral': True},
    'account.data_account_type_current_liabilities': 'account_type_current_liabilities',
    'account.data_account_type_equity': 'account_type_equity',
    'account.data_account_type_revenue': {'type': {'ref': 'account_type_revenue'}, 'kind': 'revenue'},
    'account.data_account_type_expenses': {'type': {'ref': 'account_type_expenses'}, 'kind': 'expense'},
    'account.data_account_type_direct_costs': 'account_type_direct_costs',
}

EXTRA_ACCOUNT_TYPE = {
}

def parse_code(code):
    return {
            'class': code[0],
            'group': code[0:2],
            'account': code[0:4],
            'subaccount': code[0:6]
    }

def parse_csv(filename):
    with open(filename) as f:
        reader = csv.reader(f)
        header = None
        for row in reader:
            if header == None:
                header = row
            else:
                yield dict(zip(header, row))

def cfield(parent, name, value):
        field = SubElement(parent,'field')
        field.set('name', name)
        if value == True:
            field.set('eval', 'True')
        elif value == False:
            field.set('eval', 'False')
        else:
            if isinstance(value, dict):
                for k in value:
                    field.set(k, value[k])
            else:
                field.text=value

def prettyprint(dom):        
    rough_string = tostring(dom, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

## GENERACION DE CUENTAS
trytonaccounts = Element('tryton')
trytonaccounts.append(Comment("Don't edit this file manually autogenerate from totryton.py"))
datae = SubElement(trytonaccounts, 'data')
puc_tree = defaultdict(dict)
puc_accounts = []

for row in parse_csv('account.hierarchy.csv'):
    code = parse_code(row['code'])

    name = row['name'].strip()
    key = None
    parent = None
    if code['account'].endswith("00"):
        if code['group'].endswith("0"):
            key = code['class']
        else:
            key = code['group']
            parent = code['class']
    else:
        key = code['account']
        parent = code['group']

    if not key in puc_tree:
        puc_tree[key] = {}
        puc_tree[key]['name'] = name
        puc_tree[key]['parent'] = parent
        record = SubElement(datae, 'record')
        record.set('id', "co_puc_%s" % (key))
        record.set('model', 'account.account.template')
        cfield(record, 'name', name)
        fields = {
            'ref': "co_puc_%s" %(key)
        }
        cfield(record, 'code', key)
        cfield(record, 'kind', 'view')
        if parent is None:
            parent = 'root'
        cfield(record, 'parent', {'ref': "co_puc_%s" % (parent)})
    puc_accounts.append(code['account'])

for row in parse_csv('account.account.template.csv'):
    record = SubElement(datae, 'record')
    record.set('id', row['id'])
    record.set('model', 'account.account.template')
    

    fields = {
        'name': row['name'],
        'code': row['code'],
        'deferral': False,
        'kind': 'other',
        'party_required': {'eval': 'True'}
    }


    usertype = ODOO_TO_TRYTON_ACCOUNT_TYPE[row['user_type_id:id']]
    if isinstance(usertype, dict):
        for var in usertype:
            fields[var] = usertype[var]
    else:            
        fields['type'] = {'ref': usertype}

        
    if row['reconcile'] == 'TRUE':
        fields['reconcile'] = True
    if row['reconcile'] == 'FALSE':
        fields['reconcile'] = False

    code = parse_code(row['code'])
    fields['parent'] = {'ref': "co_puc_%s" % (code['account'])}
    if not (code['account'] in puc_accounts):
        raise Exception("account %s not found " % (code['account']))
    
    for k in fields:
        cfield(record, k, fields[k])

    if row['code'] in EXTRA_ACCOUNT_TYPE:
        for k in EXTRA_ACCOUNT_TYPE[row['code']]:
            cfield(record, k, EXTRA_ACCOUNT_TYPE[row['code']][k])
            
with open('accounts.xml', 'w') as f:
    f.write(prettyprint(trytonaccounts))
    
# GENERACION DE IMPUESTOS
trytontaxs = Element('tryton')
trytontaxs.append(Comment("Don't edit this file manually autogenerate from totryton.py"))
datae = SubElement(trytontaxs, 'data')

#@deprecated
for row in parse_csv('account.account.tag.csv'):
    continue
    record = SubElement(datae, 'record')
    record.set('id', row['id'])
    record.set('model', 'account.tax.code.template')

    if row['applicability'] != 'taxes':
        raise RuntimeError("applicability unknown")
    
    cfield(record, 'name', row['name'])
    cfield(record, 'code', row['name'])
    cfield(record, 'parent', {'ref': 'vat_code_chart_root'})
    cfield(record, 'account', {'ref': 'co_puc_root'})

def tax_xml_append_group(row):
    record = SubElement(datae, 'record')
    record.set('id', row['id'])
    record.set('model', 'account.tax.group')
    cfield(record, 'name', row['name'])
    cfield(record, 'code', row['id'])

    for children_tax_id in row['children_tax_ids:id'].split(','):
        crecord = SubElement(datae, 'record')
        crecord.set('id', children_tax_id)
        cfield(crecord, 'group', {'ref': row['id']})
        
nline = 0
for row in parse_csv('account.tax.template.csv'):
    nline += 1
    amount_type = row['amount_type']
    
    if amount_type == 'group':
        #tax_xml_append_group(row)
        continue
    elif not amount_type in ['percent', 'fixed']:
        print("Warning [tax] omiting line {} unknow amount type {}".format(nline, amount_type))
        continue


    record = SubElement(datae, 'record')
    record.set('id', row['id'])
    record.set('model', 'account.tax.template')
    cfield(record, 'name', row['name'])
    cfield(record, 'description', row['name'])
    
    amount_type = row['amount_type']
    amount = decimal.Decimal(row['amount'])
    if amount_type == 'percent':
        cfield(record,'type', 'percentage')
        amount = amount / 100
        cfield(record, 'rate', {'eval': "Decimal('{0:.10g}')".format(amount)})
    elif amount_type == 'fixed':
        cfield(record, 'type', 'fixed')
        cfield(record, 'amount', {'eval': "Decimal('{0:.10g}')".format(amount)})
    else:
        raise RuntimeError("unknown amount type {}".format(amount_type))

    if row['refund_account_id:id'] == '':
        raise RuntimeError("required refund_account_id:id for credit_note_account")
    
    if len(row['account_id:id']) > 0:
        cfield(record, 'invoice_account', {'ref': row['account_id:id']})
    if len(row['refund_account_id:id']) > 0:
        cfield(record, 'credit_note_account', {'ref': row['refund_account_id:id']})
    cfield(record, 'sequence', {'eval': row['sequence']})        
    #cfield(record, 'invoice_base_code', {'ref': row['tag_ids/id']})
    #cfield(record, 'credit_note_base_code', {'ref': row['tag_ids/id']})
    cfield(record, 'credit_note_base_sign', {'eval': "-1"})
    cfield(record, 'account', {'ref': 'co_puc_root'})

    type_tax_use = row['type_tax_use']
    if type_tax_use == 'sale':
        cfield(record, 'group', {'ref': 'impuesto_venta'})
    elif type_tax_use == 'purchase':
        cfield(record, 'group', {'ref': 'impuesto_compra'})
    else:
        cfield(record, 'group', {'ref': 'impuesto_general'})
with open('taxs.xml', 'w') as f:
    f.write(prettyprint(trytontaxs))
        
